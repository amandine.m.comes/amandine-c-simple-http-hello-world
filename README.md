# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 
# Lancer le projet avec Docker

Ce projet contient un Dockerfile à sa racine. 
L'objectif est de construire une image à partir de ce Dockerfile et de lancer un container, permettant ainsi de lancer le projet.

Pour construire l'image :

- En ligne de commandes, en se positionnant dans le dossier du projet, entrer la commande :
docker image build -t <nom_image> <destination>
- Exemple : docker image build -t helloimage .

Pour lancer le container : 

- Toujours en ligne de commandes : docker run --name <nom_container> -p 8181:8181 -d <nom_image>
- Exemple : docker run --name hellocontainer -p 8181:8181 -d helloimage

- Attention : le port d'écoute est le 8181.

Vérifier que le projet fonctionne : 

- Vérifier que le container est bien en run (dans Docker Desktop par exemple)
- Lancer une requête HTTP (dans Insomnia par exemple, créer une méthode GET avec comme url : http://localhost:8181/hello)
